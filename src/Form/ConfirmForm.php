<?php

namespace Drupal\certificatelogin\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RenderInterface;
use Drupal\Core\Url;


/**
 * Confirm the user wants to create an account associated with their
 * certificate.
 *
 * @package Drupal\certificatelogin\Form
 */
class ConfirmForm extends ConfirmFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'certificatelogin_confirm_form';
  }

  public function getQuestion() {
    $site_name = $this->config('system.site')->get('name');
    return $this->t('It looks like you do not have an account registered on @s yet. Do you want to create an account named @name associated with this certificate to login?',
      ['@s' => $site_name]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  public function getCancelUrl() {
    return new Url('<front>');
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('certificatelogin.login', [], [
      'query' => [ 'confirm' => 1 ]
    ]);
  }
}
