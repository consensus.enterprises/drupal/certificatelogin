<?php

namespace Drupal\certificatelogin\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides main login block.
 *
 * @Block(
 *   id = "certificatelogin_block",
 *   admin_label = @Translation("Certificate Login Block")
 * )
 */
class CertificateLoginBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    # two links: one for UserID login (/certificatelogin) and one for direct login (/certificatelogin/login)
    $options = array('attributes' => ['class' => ['certificatelogin-link']]);
    $links = array(
      'useridlogin' => Link::fromTextAndUrl(t('Login with UserID'), Url::fromRoute('certificatelogin.initial', [], $options)),
      'certificatelogin' => Link::fromTextAndUrl(t('Login with Certificate'), Url::fromRoute('certificatelogin.login', [], $options)),
    );

    $block = array(
      '#links' => $links,
      '#theme' => 'certificatelogin_block',
      '#attributes' => [
        'class' => ['certificatelogin'],
        'id' => 'certificatelogin-block',
      ],
      '#attached' => [
        'library' => [
          'certificatelogin/certificatelogin',
        ],
      ],
    );

    return $block;
  }
}
