<?php

namespace Drupal\certificatelogin\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Certification Authority Signature Verification plugins.
 */
abstract class CaSignatureVerificationPluginBase extends PluginBase implements CaSignatureVerificationPluginInterface {


  // Add common methods and abstract methods for your plugin type here.

}
